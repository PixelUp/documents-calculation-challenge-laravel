# Setup

- Install composer dependencies
- Run tests `php artisan test`
- Start server `php artisan serve` (use 127.0.0.1 because of CORS)
