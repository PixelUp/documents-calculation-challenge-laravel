<?php

namespace Tests\Unit;

use App\Exceptions\DocumentParentIdNotFoundException;
use App\Filters\Invoice\ParentDocumentExists;
use App\Support\Invoice;
use PHPUnit\Framework\TestCase;

class ParentDocumentExistTest extends TestCase
{
    /** @test */
    public function can_detect_parent_correct_document()
    {
        $csv = [
            'Vendor 1,123456789,1000000257,1,,USD,400',
            'Vendor 1,123456789,1000000257,1,,USD,400'
        ];
        $invoices = array_map(fn($csv): Invoice => Invoice::fromRequest($csv), $csv);

        $class = new ParentDocumentExists();

        $request['invoices'] = $invoices;

        $result = $class->handle($request, function () { });

        $this->assertNull($result);
    }

    /** @test */
    public function can_detect_parent_missing_document()
    {
        $csv = [
            'Vendor 1,123456789,1000000257,1,,USD,400',
            'Vendor 1,123456789,1000000257,1,,USD,400',
            'Vendor 3,123465123,1000000263,3,99999,EUR,100'
        ];
        $invoices = array_map(fn($csv): Invoice => Invoice::fromRequest($csv), $csv);

        $class = new ParentDocumentExists();

        $request['invoices'] = $invoices;

        $this->expectException(DocumentParentIdNotFoundException::class);
        $class->handle($request, function () { });

    }
}
