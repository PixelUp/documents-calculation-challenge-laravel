<?php

namespace Tests\Unit;

use App\Actions\ExtractCurrenciesRates;
use PHPUnit\Framework\TestCase;

class ExtractCurrenciesRatesTest extends TestCase
{

    /** @test */
    public function can_extract_currency_and_rates_from_string()
    {
        $result = ExtractCurrenciesRates::execute('USD:1.23,EUR:4.56');

        $this->assertSame('USD', $result['USD']->currency);
        $this->assertSame(1.23, $result['USD']->rate);

        $this->assertSame('EUR', $result['EUR']->currency);
        $this->assertSame(4.56, $result['EUR']->rate);
    }
}
