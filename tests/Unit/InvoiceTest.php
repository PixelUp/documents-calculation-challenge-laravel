<?php

namespace Tests\Unit;

use App\Support\Invoice;
use PHPUnit\Framework\TestCase;

class InvoiceTest extends TestCase
{
    /** @test */
    public function can_create_invoice_instance_from_csv_line()
    {
        $request = 'Vendor 1,123456789,1000000257,1,,USD,400';
        $invoice = Invoice::fromRequest($request);

        $this->assertSame('Vendor 1', $invoice->customer);
        $this->assertSame(400.0, $invoice->total);
        $this->assertSame(Invoice::TYPE_INVOICE, $invoice->type);
        $this->assertSame('', $invoice->parent_document_number);
    }
}
