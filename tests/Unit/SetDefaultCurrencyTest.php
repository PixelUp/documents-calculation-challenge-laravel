<?php

namespace Tests\Unit;

use App\Actions\ExtractCurrenciesRates as Extractor;
use App\Exceptions\DefaultCurrencyNotFoundException;
use App\Filters\Invoice\SetDefaultCurrency;
use PHPUnit\Framework\TestCase;

class SetDefaultCurrencyTest extends TestCase
{
    /** @test */
    public function can_set_default_currency()
    {
        $input = 'EUR:1,USD:0.987,GBP:0.878';

        $currencies = Extractor::execute($input);
        $request['currencies'] = $currencies;

        $class = new SetDefaultCurrency();
        $result = $class->handle($request, function () { });
        $this->assertNull($result);
    }

    /** @test */
    public function will_catch_missing_default_currency()
    {
        $input = 'EUR:0,USD:0.987,GBP:0.878';

        $currencies = Extractor::execute($input);
        $request['currencies'] = $currencies;

        $class = new SetDefaultCurrency();

        $this->expectException(DefaultCurrencyNotFoundException::class);
        $class->handle($request, function () { });

    }
}
