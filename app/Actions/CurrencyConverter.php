<?php

namespace App\Actions;

use App\Models\Invoice\CurrencyRate;
use App\Support\Invoice;

class CurrencyConverter
{
    private array $currencies;
    private CurrencyRate $default_currency;
    private Invoice $invoice;

    public function setCurrencies(array $currencies): void
    {
        $this->currencies = $currencies;
    }

    public function to(CurrencyRate $default_currency): void
    {
        $this->default_currency = $default_currency;
    }

    public function from(Invoice $invoice): void
    {
        $this->invoice = $invoice;
    }

    public function calculate(): float|int
    {
        /** @var CurrencyRate $from */
        $from = $this->currencies[$this->default_currency->currency];
        /** @var CurrencyRate $to */
        $to = $this->currencies[$this->invoice->currency];

        $amount = $this->invoice->total;

        return $amount * ($to->rate / $from->rate);
    }


}
