<?php

namespace App\Actions;

use App\Models\Invoice\CurrencyRate;

class ExtractCurrenciesRates
{

    public static function execute(string $inputCurrencies): array
    {
        $currencies = explode(',', $inputCurrencies);

        $rates = [];

        foreach ($currencies as $currency) {
            $currency_rates = explode(':', $currency);
            $rates[$currency_rates[0]] = new CurrencyRate($currency_rates[0], $currency_rates[1]);
        }

        return $rates;
    }
}
