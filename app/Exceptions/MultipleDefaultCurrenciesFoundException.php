<?php

namespace App\Exceptions;

use Exception;

class MultipleDefaultCurrenciesFoundException extends Exception
{
    public function __construct()
    {
        parent::__construct('Multiple default currencies found.');
    }
}
