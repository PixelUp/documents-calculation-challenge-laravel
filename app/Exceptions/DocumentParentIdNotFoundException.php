<?php

namespace App\Exceptions;

use Exception;

class DocumentParentIdNotFoundException extends Exception
{
    public function __construct(string $document_number, string $parent_document_number)
    {
        parent::__construct("Document parent id {$parent_document_number} not found for document number: {$document_number}");
    }
}
