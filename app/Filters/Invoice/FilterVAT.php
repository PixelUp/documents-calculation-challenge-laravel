<?php

namespace App\Filters\Invoice;

use App\Support\Invoice;
use Closure;

class FilterVAT
{
    public function __construct(private string $filter) { }

    public function handle($request, Closure $next)
    {
        if ($this->filter === 'undefined' || blank($this->filter)) {
            return $next($request);
        }

        $invoices = array_filter($request['invoices'], fn(Invoice $invoice) => $invoice->vat === $this->filter) ?? [];

        $request['invoices'] = $invoices;
        return $next($request);
    }
}
