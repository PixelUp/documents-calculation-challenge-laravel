<?php

namespace App\Filters\Invoice;

use App\Exceptions\DocumentParentIdNotFoundException;
use App\Support\Invoice;
use Closure;

class ParentDocumentExists
{
    public function handle(array $request, Closure $next)
    {
        //check if $request array has a parent_document_number key matching an existing document_number

        /** @var Invoice[] $invoices */
        $invoices = $request['invoices'];
        $document_ids = array_map(fn(Invoice $invoice) => $invoice->document_number, $invoices);

        // remove empty or duplicate values
        $document_ids = array_unique(array_filter($document_ids));

        // validate if all document ids are available from parent document column
        foreach ($invoices as $invoice) {

            if (blank($invoice->parent_document_number)) {
                continue;
            }

            if (!in_array($invoice->parent_document_number, $document_ids)) {
                throw new DocumentParentIdNotFoundException($invoice->document_number, $invoice->parent_document_number);
            }
        }

        return $next($request);
    }
}
