<?php

namespace App\Filters\Invoice;

use App\Exceptions\DefaultCurrencyNotFoundException;
use App\Exceptions\MultipleDefaultCurrenciesFoundException;
use App\Models\Invoice\CurrencyRate;
use Closure;

class SetDefaultCurrency
{
    public function handle(array $request, Closure $next)
    {
        $default_currency = array_filter($request['currencies'], fn(CurrencyRate $currencyRate) => $currencyRate->rate === 1.0);

        if (count($default_currency) > 1) {
            throw new MultipleDefaultCurrenciesFoundException();
        }

        if (!$default_currency) {
            throw new DefaultCurrencyNotFoundException();
        }

        // reset array key to return only the default currency
        $request['default_currency'] = array_shift($default_currency);

        return $next($request);
    }
}
