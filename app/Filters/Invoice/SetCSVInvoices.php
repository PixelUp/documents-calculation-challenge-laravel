<?php

namespace App\Filters\Invoice;

use App\Support\Invoice;
use Closure;

class SetCSVInvoices
{
    public function __construct(private string $invoice) { }

    public function handle($request, Closure $next)
    {
        $rows = str_getcsv($this->invoice, PHP_EOL);

        // remove first row - header
        array_shift($rows);

        $invoices = array_map(fn($invoice) => Invoice::fromRequest($invoice), $rows);

        $request['invoices'] = $invoices;
        return $next($request);
    }
}
