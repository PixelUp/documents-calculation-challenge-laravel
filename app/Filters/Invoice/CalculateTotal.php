<?php

namespace App\Filters\Invoice;

use App\Actions\CurrencyConverter;
use App\Support\Invoice;
use Closure;

class CalculateTotal
{
    public function handle(array $request, Closure $next)
    {
        /** @var Invoice[] $invoices */
        $invoices = $request['invoices'];
        $converter = new CurrencyConverter();
        $converter->setCurrencies($request['currencies']);

        $total_amount = 0;
        foreach ($invoices as $invoice) {

            $converter->from($invoice);
            $converter->to($request['default_currency']);

            $total = $converter->calculate();

            match ($invoice->type) {
                Invoice::TYPE_CREDIT => $total_amount -= $total,
                Invoice::TYPE_DEBIT => $total_amount += $total,
                default => $total_amount += $total
            };
        }

        // round $total_amount to 2 decimal places
        $request['total'] = round($total_amount, 2);
        $request['total_text'] = "Total: {$request['total']} {$request['output_currency']}";
        return $next($request);
    }
}
