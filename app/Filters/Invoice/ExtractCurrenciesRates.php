<?php

namespace App\Filters\Invoice;

use App\Actions\ExtractCurrenciesRates as Extractor;
use Closure;

class ExtractCurrenciesRates
{
    public function handle(array $request, Closure $next)
    {
        $request['currencies'] = Extractor::execute($request['input_currencies']);
        return $next($request);
    }
}
