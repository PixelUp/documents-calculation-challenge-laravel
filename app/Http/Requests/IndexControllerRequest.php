<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndexControllerRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'file'       => ['required', 'file', 'mimes:csv,txt', 'max:10000'],
            'input_currencies' => ['required', 'min:2'],
            'output_currency'     => ['required', 'string', 'min:2', 'max:50'],
            'filter_vat' => ['nullable'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
