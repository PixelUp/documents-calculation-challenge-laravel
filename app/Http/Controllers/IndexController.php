<?php

namespace App\Http\Controllers;

use App\Filters\Invoice\CalculateTotal;
use App\Filters\Invoice\ExtractCurrenciesRates;
use App\Filters\Invoice\FilterVAT;
use App\Filters\Invoice\ParentDocumentExists;
use App\Filters\Invoice\SetCSVInvoices;
use App\Filters\Invoice\SetDefaultCurrency;
use App\Http\Requests\IndexControllerRequest;
use Illuminate\Routing\Pipeline;

class IndexController extends Controller
{
    public function __invoke(IndexControllerRequest $request)
    {
        $validated = $request->validated();
        $csv = file_get_contents($request->file('file')->getRealPath());
        $validated['file'] = $csv;
        $pipes = [
            new SetCSVInvoices($csv),
            ParentDocumentExists::class,
            ExtractCurrenciesRates::class,
            SetDefaultCurrency::class
        ];

        if ($vat = $request->get('filter_vat')) {
            $pipes[] = new FilterVAT($vat);
        }

        // final pipe is to calculate total
        $pipes[] = CalculateTotal::class;

        try {
            $result = app(Pipeline::class)
                ->send($validated)
                ->through($pipes)
                ->thenReturn();
            return response()->json(['total' => $result['total_text'], 'success' => true]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'success' => false]);
        }

    }
}
