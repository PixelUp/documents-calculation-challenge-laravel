<?php

namespace App\Support;


class Invoice
{
    public const TYPE_INVOICE = 1;
    public const TYPE_CREDIT  = 2;
    public const TYPE_DEBIT   = 3;

    public function __construct(
        public string $customer,
        public string $vat,
        public string $document_number,
        public int    $type,
        public string $parent_document_number,
        public string $currency,
        public float  $total
    )
    {
    }

    /**
     *
     * @param string $request
     *
     * @return Invoice
     */
    public static function fromRequest(string $request): self
    {
        [$customer, $vat, $document_number, $type, $parent_document_number, $currency, $total] = str_getcsv($request);
        return new static($customer, $vat, $document_number, $type, $parent_document_number, $currency, $total);
    }
}
