<?php

namespace App\Models\Invoice;

class CurrencyRate
{
    public function __construct(public string $currency, public float $rate) { }
}
